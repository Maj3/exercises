#include<iostream>
using namespace std;

void FillArray2D(int n_rows,int n_columns,int **array)
{
    int n=1;
    for (int i = 0; i < n_rows; i++)
    {
        for (int j = 0;j<n_columns;j++)
        {
            array[i][j] = n;
            n++;
        }
    }
}

int **NewArray2D(int n_rows, int n_columns)
{
    if(n_rows<1 || n_columns<1)
        return nullptr;
    int **tab = new int *[n_rows];
    for(int i=0;i<n_rows;i++)
    {
        tab[i]=new int [n_columns];
    }
    FillArray2D(n_rows,n_columns,tab);
    return tab;
}


void PrintArray2D(int **array, int n_rows, int n_columns)
{
    for(int i=0;i<n_columns;i++)
    {
        for(int j=0;j<n_rows;j++)
            cout << array[j][i] << '\t';
        cout << '\n';
    }
}

void DeleteArray2D(int **array, int n_rows, int n_columns)
{
    for(int i=0;i<n_rows;i++)
    {
        delete [] array[i];
    }
    delete [] array;
}