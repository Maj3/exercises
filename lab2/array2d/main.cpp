#include<iostream>
#include "Array2D.h"
using namespace std;

int main()
{
    int rows, columns;
    int **array;
    cout << "Podaj ilosc wierszy: ";
    cin >> rows;
    cout << "Podaj ilosc kolumn: ";
    cin >> columns;
    array= NewArray2D(rows, columns);
    PrintArray2D(array,rows,columns);
    DeleteArray2D(array,rows,columns);
    return 0;
}