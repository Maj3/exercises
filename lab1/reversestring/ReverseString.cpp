//
// Created by grucmaja on 28.02.17.
//
using namespace std;
#include "ReverseString.h"

string reverse(string str)
{
    size_t size = str.size();
    char reversed_characters[size+1];
    for(int i=size-1;i>=0;i--)
    {
        reversed_characters[size-i-1]=str[i];
    }
    reversed_characters[size]='\0';
    return std::string(reversed_characters);
}