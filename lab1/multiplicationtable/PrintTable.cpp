//
// Created by maj3 on 03.03.17.
//
#include "MultiplicationTable.h"
#include "iostream"
using namespace std;

void PrintTable(int tab[][10])
{
    for(int i=0;i<10;i++)
    {
        for(int j=0;j<10;j++)
        {
            cout << tab[i][j] << "\t";
        }
        cout << endl;
    }
}
