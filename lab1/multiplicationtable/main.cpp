//
// Created by maj3 on 03.03.17.
//

#include "MultiplicationTable.h"

int main()
{
    int tab[10][10];
    MultiplicationTable(tab);
    PrintTable(tab);
    return 0;
}