//
// Created by maj3 on 03.03.17.
//
#include <iostream>

using namespace std;

uint64_t DoubleBasePalindromes(int max_vaule_exculsive)
{
    int sum = 0;
    for(int i=1;i<=max_vaule_exculsive;i++)
    {
        string str = to_string(i);
        size_t size = str.size();
        bool decimal = true;
        for(int i=0;i<size/2;i++)
        {
            if(str[i]==str[size-1-i])
                continue;
            else
            {
                decimal = false;
                break;
            }
        }
        if(decimal == true)
        {
            string bin;
            int temp = i;
            while(temp!=0)
            {
                bin.push_back(temp%2);
                temp /= 2;
            }
            bool binary = true;
            size_t size_bin = bin.size();
            for(int j=0;j<size_bin/2;j++)
            {
                if(bin[j]==bin[size_bin-1-j])
                    continue;
                else
                {
                    binary = false;
                    break;
                }
            }
            if(binary == true)
                sum += i;

        }
    }
    return sum;
}