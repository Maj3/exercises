//
// Created by maj3 on 03.03.17.
//
#include <iostream>
#include "Palindrome.h"
using namespace std;

bool is_palindrome(string str)
{
    size_t size = str.size();
    for(int i=0;i<size/2;i++)
    {
        if(str[i]==str[size-1-i])
            continue;
        else
            return false;
    }
    return true;
}