//
// Created by mwypych on 02.02.17.
//
#include "Factorial.h"

int factorial(int value) {
  if (value>=13 || value<=-13)
    return 0;
  else {
    if (value == 0 || value == 1 || value == -1) {
      if (value == 0 || value == 1)
        return 1;
      else
        return -1;
    } else {
      if (value > 0) {
        return factorial(value - 1) * value;
      } else {
        return factorial(value + 1) * value;
      }
    }
  }
}